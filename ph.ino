#include <Wire.h>
#define address 99 //99 is dec from hex 63
byte code = 0;
char ph_data[20];
byte in_char = 0;
byte i = 0;
int time_ = 825;
float ph_float;

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
Wire.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  Wire.beginTransmission(address);
  Wire.write('r');
  Wire.endTransmission();
  delay(time_);
  Wire.requestFrom(address, 20 ,1);
  code = Wire.read();
  
  if(code==2 || code==255){
    Serial.println("Failed");
    return;
  }
  while (Wire.available()) {
    in_char = Wire.read();
    ph_data[i]=in_char;
    i +=1;
    if(in_char ==0) {
      i = 0;

      break;
    }
  }
  Serial.println(ph_data);
  delay(2000-time_);
}
