
  // put your setup code here, to run once:
#include <Wire.h>
#define address 99 //99 is dec from hex 63
//for pump
float level=0;//speed of motor
int motor_pin_1=9;//PWM pin
int motor_pin_2=8;
int max_ = 100;//max speed, will probably need to be reduced
int min_=0;//min speed
//for ph read
byte code = 0;//checks for error in read
char ph_data[20];//stores the data from probe in array
byte in_char = 0;//character from i2c 
byte i = 0;//indexing number
int time_ = 815;//used as a default delay

float ph_current;
float ph_target=7;
float ph_media_1=4;
float ph_media_2=10;
void setup() {
pinMode(motor_pin_1,OUTPUT);
pinMode(motor_pin_2,OUTPUT);
Serial.begin(9600);
Wire.begin();
analogWrite(motor_pin_1,255);
analogWrite(motor_pin_2,255);
delay(500);//fills the tube 
analogWrite(motor_pin_1,0);
analogWrite(motor_pin_2,0);
}

void loop() {
  // put your main code here, to run repeatedly:
ph_current = ph_read();
Serial.print("PH:");
Serial.println(ph_current);
delay(time_);
level = control(ph_target,ph_current);
setmotor(level);
}

float ph_read(){
  float ph_float;//output
  Wire.beginTransmission(address);
  Wire.write('r');
  Wire.endTransmission();
  delay(time_);//required delay for the chip to output
  Wire.requestFrom(address, 20 ,1);//reads 20 
  code = Wire.read();
  
  if(code==2 || code==255){
    Serial.println("Failed");
    return -1;
  }
  while (Wire.available()) {//places int in array until no more sig figs
    in_char = Wire.read();
    ph_data[i]=in_char;
    i +=1;
    if(in_char ==0) {
      i = 0;
      break;
    }
  }
   ph_float=atof(ph_data);//array to float
   //Serial.println(ph_float,3);
   return ph_float;//return the value
}

float control(float target, float current) {
  float diff = target-current;
  if (abs(diff)<0.3){
    diff=0;
    }
  Serial.print("diff:");
  Serial.println(diff);
  float diff_range = map(diff*10,-140,140,-100,100);
  float mapped=diff_range/100;
  Serial.print("mapped:");
  Serial.println(mapped);
  return mapped; 
}
float setmotor(float mapped){
  int pump;
  Serial.print("Pump:");
  if (mapped<0){
    pump=motor_pin_1;
    Serial.println("Pump 0");
  }
  else if (mapped>0){
    pump=motor_pin_2;
    Serial.println("Pump 1");
  }
  else{
    Serial.println("None");
    return;
  }
  analogWrite(pump,150);
  delay(1000*abs(mapped));
  analogWrite(pump,0);
 
}
